# no default rules
.SUFFIXES:

POLICY_NAME ?= test-policy
BINDIR ?= /bin
SBINDIR ?= /sbin
SYSCONFDIR ?= /etc
POLICY_VERSION ?= 32
SECILC ?= secilc
SEMODULE ?= semodule
SELINUX_CONFDIR ?= $(SYSCONFDIR)/selinux/$(POLICY_NAME)
DESTDIR ?= /

MODULES_BASE = $(sort $(shell find policy/base -name "*.cil"))

.PHONY: all
all: policy.$(POLICY_VERSION)

.PHONY: clean
clean:
	rm -f policy.$(POLICY_VERSION) file_contexts

policy.%: $(MODULES_BASE)
	$(SECILC) -vvv --policyvers=$* --output="$@" $^

file_contexts: policy.$(POLICY_VERSION)

.PHONY: install
install: install-config install-semodule

.PHONY: install-semodule
install-semodule:
	install --directory $(DESTDIR)$(SHAREDSTATEDIR)/selinux/$(POLICY_NAME)
	$(SEMODULE) --priority=100 --noreload --store $(POLICY_NAME) --path $(DESTDIR) $(addprefix --install ,$(MODULES_BASE))

.PHONY: install-config
install-config: \
	file_contexts config/dbus_contexts config/failsafe_context config/default_contexts \
	config/default_type config/users/sysadm.user config/file_contexts.subs_dist
	install --directory $(DESTDIR)$(SELINUX_CONFDIR)/contexts/files
	install --mode 0644 file_contexts $(DESTDIR)$(SELINUX_CONFDIR)/contexts/files/file_contexts
	install --directory $(DESTDIR)$(SELINUX_CONFDIR)/contexts/users
	install --mode 0644 config/dbus_contexts $(DESTDIR)$(SELINUX_CONFDIR)/contexts/dbus_contexts
	install --mode 0644 config/failsafe_context $(DESTDIR)$(SELINUX_CONFDIR)/contexts/failsafe_context
	install --mode 0644 config/default_contexts $(DESTDIR)$(SELINUX_CONFDIR)/contexts/default_contexts
	install --mode 0644 config/default_type $(DESTDIR)$(SELINUX_CONFDIR)/contexts/default_type
	install --mode 0644 config/users/sysadm.user $(DESTDIR)$(SELINUX_CONFDIR)/contexts/users/sysadm.user
	install --mode 0644 config/file_contexts.subs_dist $(DESTDIR)$(SELINUX_CONFDIR)/contexts/files/file_contexts.subs_dist
